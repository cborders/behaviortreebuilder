package productions.moo.behavior.view;

import com.google.common.eventbus.Subscribe;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Stroke;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

import javax.inject.Inject;
import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.ToolTipManager;

import productions.moo.behavior.CodeExporter;
import productions.moo.behavior.events.AddNodeEvent;
import productions.moo.behavior.events.MooEventBus;
import productions.moo.behavior.events.RequestExportEvent;
import productions.moo.behavior.nodes.NodeManager;
import productions.moo.behavior.nodes.RenderNode;

public class RenderNodeCanvas extends JComponent
{
	private RenderNode _root;

	private JPopupMenu _contextMenu;
	private JMenu _nodeMenu;
	private DeleteNodeAction _deleteAction;

	private Point _mousePoint = new Point();
	private Rectangle _mouseRect = new Rectangle();
	private boolean _selecting;

	@Inject
	private MooEventBus _eventBus;
	private NodeManager _nodeManager;

	public RenderNodeCanvas()
	{
		_eventBus = MooEventBus.getInstance();
		_eventBus.register(this);

		_nodeManager = NodeManager.getInstance();

		setOpaque(true);

		addMouseListener(new MouseHandler());
		addMouseMotionListener(new MouseMotionHandler());

		_root = _nodeManager.createNode("Selector").setPosition(new Point(50, 50));

		RenderNode seq = _nodeManager.createNode("Sequence").setPosition(new Point(100, 50));
		seq.addChild(_nodeManager.createNode("Inverter").setPosition(new Point(150, 50)));
		seq.addChild(_nodeManager.createNode("Repeater").setPosition(new Point(150, 100)));
		_root.addChild(seq);

		RenderNode par = _nodeManager.createNode("Parallel").setPosition(new Point(100, 100));
		par.addChild(_nodeManager.createNode("RepeatUntilFailure").setPosition(new Point(150, 150)));
		par.addChild(_nodeManager.createNode("Succeeder").setPosition(new Point(150, 200)));
		_root.addChild(par);

		buildContextMenu();
	}

	public RenderNode getRootNode()
	{
		return _root;
	}

	private RenderNode getNode(RenderNode node, Point position)
	{
		if (node.contains(position))
		{
			return node;
		}
		else if(node.getChildren().size() > 0)
		{
			for(RenderNode child : node.getChildren())
			{
				RenderNode found = getNode(child, position);
				if(found != null)
				{
					return found;
				}
			}
		}

		return null;
	}

	@Override
	public void paint (Graphics graphics)
	{
		Graphics2D graphics2D = (Graphics2D) graphics;
		graphics2D.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		graphics2D.setColor(new Color(0x00f0f0f0));
		graphics2D.fillRect(0, 0, getWidth(), getHeight());

		drawNode(_root, graphics2D);

		if (_selecting)
		{
			graphics.setColor(Color.darkGray);
			graphics.drawRect(_mouseRect.x, _mouseRect.y, _mouseRect.width, _mouseRect.height);
		}
	}

	private void drawNode(RenderNode node, Graphics2D graphics)
	{
		for(RenderNode child : node.getChildren())
		{
			Point p1 = node.getPosition();
			Point p2 = child.getPosition();

			Stroke oldStroke = graphics.getStroke();
			graphics.setStroke(new BasicStroke(2));
			graphics.setColor(Color.darkGray);
			graphics.drawLine(p1.x, p1.y, p2.x, p2.y);
			graphics.setStroke(oldStroke);

			drawNode(child, graphics);
		}

		node.draw(graphics);
	}

	@Subscribe
	public void exportTree(RequestExportEvent event)
	{
		CodeExporter.export(_root, "/Users/cborders/Desktop", "AnotherTest");
	}

	@Subscribe
	public void addNode(AddNodeEvent event)
	{
		addNodeType(event.node.getFullName());
	}

	private void buildContextMenu()
	{
		_contextMenu = new JPopupMenu();
		_nodeMenu = new JMenu("Add Node");
		_deleteAction = new DeleteNodeAction();

		String[] names = _nodeManager.getNodeNames();

		for(String name : names)
		{
			addNodeType(name);
		}

		_contextMenu.add(_nodeMenu);
		_contextMenu.add(_deleteAction);
	}

	private void addNodeType(String nodeName)
	{
		JMenuItem item = new JMenuItem(nodeName);
		item.setAction(new AddNodeAction(nodeName));
		_nodeMenu.add(item);
	}

	private void showPopup(MouseEvent e)
	{
		// If we are clicking on an existing node add the delete option.
		RenderNode node = getNode(_root, new Point(e.getX(), e.getY()));
		_deleteAction.setNode(node);
		_contextMenu.show(e.getComponent(), e.getX(), e.getY());
	}

	private void selectNone (RenderNode node)
	{
		node.setSelected(false);
		for (RenderNode child : node.getChildren())
		{
			selectNone(child);
		}
	}

	private boolean selectOne (Point p)
	{
		RenderNode node = getNode(_root, p);
		if (node != null)
		{
			if (!node.isSelected())
			{
				selectNone(_root);
				node.setSelected(true);
			}
			return true;
		}
		return false;
	}

	private void selectToggle (Point p)
	{
		RenderNode node = getNode(_root, p);
		if (node != null && node.contains(p))
		{
			node.setSelected(!node.isSelected());
		}
	}

	private void selectRect (RenderNode node, Rectangle rect)
	{
		node.setSelected(rect.contains(node.getPosition()));
		for (RenderNode child : node.getChildren())
		{
			selectRect(child, rect);
		}
	}

	private void deleteNode(RenderNode node, RenderNode target)
	{
		for (RenderNode child : node.getChildren())
		{
			if(child == target)
			{
				node.getChildren().remove(target);
				repaint();
				return;
			}
		}

		for (RenderNode child : node.getChildren())
		{
			deleteNode(child, target);
		}
	}

	// TODO: Move these out to another file?
	private class MouseHandler extends MouseAdapter
	{
		@Override
		public void mouseReleased (MouseEvent e)
		{
			_selecting = false;
			_mouseRect.setBounds(0, 0, 0, 0);
			e.getComponent().repaint();
		}

		@Override
		public void mousePressed (MouseEvent e)
		{
			_mousePoint = e.getPoint();
			if (e.isShiftDown())
			{
				selectToggle(_mousePoint);
			}
			else if (e.isPopupTrigger())
			{
				showPopup(e);
			}
			else if (selectOne(_mousePoint))
			{
				_selecting = false;
			}
			else
			{
				selectNone(_root);
				_selecting = true;
			}
			e.getComponent().repaint();
		}
	}

	private class MouseMotionHandler extends MouseMotionAdapter
	{
		Point _delta = new Point();

		@Override
		public void mouseDragged (MouseEvent e)
		{
			if (_selecting)
			{
				_mouseRect.setBounds(
					Math.min(_mousePoint.x, e.getX()),
					Math.min(_mousePoint.y, e.getY()),
					Math.abs(_mousePoint.x - e.getX()),
					Math.abs(_mousePoint.y - e.getY()));
				selectRect(_root, _mouseRect);
			}
			else
			{
				_delta.setLocation(
					e.getX() - _mousePoint.x,
					e.getY() - _mousePoint.y);
				updatePosition(_root, _delta);
				_mousePoint = e.getPoint();
			}
			e.getComponent().repaint();
		}

		@Override
		public void mouseMoved (MouseEvent e)
		{
			ToolTipManager.sharedInstance().mouseMoved(e);
			RenderNode node = getNode(_root, e.getPoint());
			if (node != null)
			{
				setToolTipText(node.getFullName());
			}
			else
			{
				setToolTipText("");
			}
		}

		private void updatePosition (RenderNode node, Point delta)
		{
			if(node.isSelected())
			{
				node.translate(delta);
			}
			for (RenderNode child : node.getChildren())
			{
				updatePosition(child, delta);
			}
		}
	}

	private class AddNodeAction extends AbstractAction
	{
		public String nodeName;

		public AddNodeAction(String nodeName)
		{
			super(nodeName);
			this.nodeName = nodeName;
		}

		@Override
		public void actionPerformed (ActionEvent e)
		{
			RenderNode node = _nodeManager.createNode(nodeName);
			node.setPosition(_mousePoint);

			RenderNode parent = getNode(_root, _mousePoint);
			if(parent == null)
			{
				parent = _root;
			}

			parent.addChild(node);
			RenderNodeCanvas.this.repaint();
		}
	}

	private class DeleteNodeAction extends AbstractAction
	{
		private RenderNode _node;

		public DeleteNodeAction()
		{
			super("Delete Node");
		}

		public void setNode(RenderNode node)
		{
			_node = node;
			setEnabled(_node != null);
		}

		@Override
		public void actionPerformed (ActionEvent e)
		{
			int choice = JOptionPane.showConfirmDialog(RenderNodeCanvas.this,
				"Are you sure you want to delete this " + _node.getFullName() + " node?",
				"Delete?",
				JOptionPane.YES_NO_OPTION,
				JOptionPane.PLAIN_MESSAGE);
			if(choice == JOptionPane.YES_OPTION)
			{
				deleteNode(_root, _node);
			}
		}
	}
}
