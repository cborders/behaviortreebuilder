package productions.moo.behavior.view;

import com.google.common.eventbus.Subscribe;

import javax.swing.JFrame;

import productions.moo.behavior.events.CreateNodeMenuEvent;
import productions.moo.behavior.events.MooEventBus;

public class EditorWindow extends JFrame
{
	private MooEventBus _eventBus;
	private EditorMenu _menu;
	private RenderNodeCanvas _canvas;

	public EditorWindow()
	{
		// TODO: Read app name from the Gradle file or make this the save file name
		super("Behavior Builder");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		_eventBus = MooEventBus.getInstance();
		_eventBus.register(this);

		_menu = new EditorMenu();
		_canvas = new RenderNodeCanvas();

		add(_canvas);

		pack();

		setJMenuBar(_menu);
		// TODO: Read this from some kind of config file
		setSize(400, 400);
		setLocationRelativeTo(null);
		setVisible(true);
	}

	@Subscribe
	public void showAddNodeDialog(CreateNodeMenuEvent event)
	{
		new AddNodeDialog(this);
	}
}
