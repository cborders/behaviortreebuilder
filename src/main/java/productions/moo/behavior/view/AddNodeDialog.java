package productions.moo.behavior.view;

import org.apache.commons.lang3.text.WordUtils;

import java.awt.Button;
import java.awt.Choice;
import java.awt.Color;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.GroupLayout;
import javax.swing.JColorChooser;

import productions.moo.behavior.events.CreateNodeEvent;
import productions.moo.behavior.events.MooEventBus;
import productions.moo.behavior.nodes.RenderNode;

public class AddNodeDialog extends Dialog implements ActionListener
{
	private MooEventBus _eventBus = MooEventBus.getInstance();

	private Color _nodeColor;

	private Label _packageLabel;
	private TextField _package;

	private Label _classLabel;
	private TextField _class;

	private Choice _nodeType;

	private Label _fullNameLabel;
	private TextField _fullName;

	private Label _shortNameLabel;
	private TextField _shortName;

	private Button _color;

	private Button _ok;
	private Button _cancel;

	private static final int TEXT_WIDTH = 50;
	private static final int COLOR_WIDTH = 10;

	public AddNodeDialog (Window owner)
	{
		super(owner, "Add Node Type", ModalityType.APPLICATION_MODAL);
		setResizable(false);

		_nodeColor = new Color(0xF0EF38);

		_packageLabel = new Label("Package");
		_package = new TextField(TEXT_WIDTH);

		_classLabel = new Label("Class Name");
		_class = new TextField(TEXT_WIDTH);

		_nodeType = new Choice();
		_nodeType.add("Composite");
		_nodeType.add("Decorator");
		_nodeType.add("Leaf");

		_fullNameLabel = new Label("Full Name");
		_fullName = new TextField(TEXT_WIDTH);

		_shortNameLabel = new Label("Short Name");
		_shortName = new TextField(TEXT_WIDTH);

		_color = new Button();
		_color.setMaximumSize(new Dimension(COLOR_WIDTH, COLOR_WIDTH));
		_color.setBackground(_nodeColor);
		_color.setForeground(null);
		_color.addActionListener(this);

		_ok = new Button("Ok");
		_ok.addActionListener(this);
		_cancel = new Button("Cancel");
		_cancel.addActionListener(this);

		addWindowListener(new WindowAdapter()
		{
			public void windowClosing (WindowEvent windowEvent)
			{
				dispose();
			}
		});

		setBounds(0, 0, 300, 200);

		Panel panel = new Panel();
		GroupLayout layout = new GroupLayout(panel);
		panel.setLayout(layout);
		layout.setAutoCreateGaps(true);
		layout.setAutoCreateContainerGaps(true);

		layout.setVerticalGroup(
			layout.createSequentialGroup()
				.addComponent(_packageLabel)
				.addComponent(_package)
				.addComponent(_classLabel)
				.addComponent(_class)
				.addComponent(_nodeType)
				.addComponent(_fullNameLabel)
				.addComponent(_fullName)
				.addComponent(_shortNameLabel)
				.addComponent(_shortName)
				.addComponent(_color)
				.addGroup(
					layout.createParallelGroup(GroupLayout.Alignment.LEADING)
						.addComponent(_cancel)
						.addComponent(_ok)
				)
		);

		layout.setHorizontalGroup(
			layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addComponent(_packageLabel)
				.addComponent(_package)
				.addComponent(_classLabel)
				.addComponent(_class)
				.addComponent(_nodeType)
				.addComponent(_fullNameLabel)
				.addComponent(_fullName)
				.addComponent(_shortNameLabel)
				.addComponent(_shortName)
				.addComponent(_color)
				.addGroup(
					layout.createSequentialGroup()
						.addComponent(_cancel)
						.addComponent(_ok)
				)
		);

		add(panel);
		pack();
		setVisible(true);
	}

	@Override
	public void actionPerformed (ActionEvent e)
	{
		if(e.getSource() == _ok)
		{
			if(_package.getText() == null || _package.getText().isEmpty() ||
			_class.getText() == null || _class.getText().isEmpty() ||
			_fullName.getText() == null || _fullName.getText().isEmpty() ||
			_shortName.getText() == null || _shortName.getText().isEmpty())
			{
				// TODO: show some kind of an alert
			}
			else
			{
				CreateNodeEvent event = new CreateNodeEvent();
				event.color = _nodeColor;
				event.packageName = _package.getText();
				event.className = _class.getText();
				event.nodeType = _nodeType.getSelectedItem();
				event.fullname = _fullName.getText();
				event.shortName = _shortName.getText();

				_eventBus.post(event);
				dispose();
			}
		}
		else if(e.getSource() == _cancel)
		{
			dispose();
		}
		else if(e.getSource() == _color)
		{
			_nodeColor = JColorChooser.showDialog(this, "Demo", _nodeColor);
			_color.setBackground(_nodeColor);

		}
	}
}
