package productions.moo.behavior.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.inject.Inject;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import productions.moo.behavior.events.CreateNodeMenuEvent;
import productions.moo.behavior.events.MooEventBus;
import productions.moo.behavior.events.RequestExportEvent;

public class EditorMenu extends JMenuBar
{
	@Inject
	private MooEventBus _eventBus;
	private MenuItemListener _menuListener;

	public EditorMenu()
	{
		_eventBus = MooEventBus.getInstance();
		_menuListener = new MenuItemListener();

		JMenu fileMenu = new JMenu("File");

		JMenuItem export = new JMenuItem("Export...");
		export.setActionCommand("Export");
		export.addActionListener(_menuListener);
		fileMenu.add(export);

//		_saveMenu = new MenuItem("Save");
//		_saveMenu.setActionCommand("Save");
//		_saveMenu.addActionListener(_menuListener);
//		_saveMenu.setEnabled(_mainWindow.getSaveFile() != null);
//		fileMenu.add(_saveMenu);
//
//		MenuItem saveAs = new MenuItem("Save as...");
//		saveAs.setActionCommand("SaveAs");
//		saveAs.addActionListener(_menuListener);
//		fileMenu.add(saveAs);
//
//		fileMenu.addSeparator();
//
//		MenuItem addCustomNode = new MenuItem("Add Custom Node...");
//		addCustomNode.setActionCommand("CustomNode");
//		addCustomNode.addActionListener(_menuListener);
//		fileMenu.add(addCustomNode);

		add(fileMenu);

		JMenu nodeMenu = new JMenu("Nodes");

		JMenuItem addNode = new JMenuItem("Add Node...");
		addNode.setActionCommand("AddNode");
		addNode.addActionListener(_menuListener);
		nodeMenu.add(addNode);

		add(nodeMenu);
	}

	private class MenuItemListener implements ActionListener
	{
		public void actionPerformed (ActionEvent e)
		{
			String actionCommand = e.getActionCommand();

			if (actionCommand.equalsIgnoreCase("Export"))
			{
				_eventBus.post(new RequestExportEvent());
			}
			else if (actionCommand.equalsIgnoreCase("AddNode"))
			{
				_eventBus.post(new CreateNodeMenuEvent());
			}
		}
	}
}
