package productions.moo.behavior.events;

import java.awt.Color;

public class CreateNodeEvent
{
	public Color color;
	public String packageName;
	public String className;
	public String nodeType;
	public String fullname;
	public String shortName;
}
