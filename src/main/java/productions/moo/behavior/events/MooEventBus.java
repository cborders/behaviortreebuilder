package productions.moo.behavior.events;

import com.google.common.eventbus.EventBus;

import javafx.beans.binding.ObjectExpression;

public class MooEventBus
{
	private static MooEventBus _instance;
	public static MooEventBus getInstance()
	{
		if(_instance == null) { new MooEventBus(); }
		return _instance;
	}

	private final EventBus _eventBus;

	private MooEventBus()
	{
		_instance = this;
		_eventBus = new EventBus();
	}

	public void register(Object listener)
	{
		_eventBus.register(listener);
	}

	public void unregister(Object listener)
	{
		_eventBus.unregister(listener);
	}

	public void post(Object event)
	{
		_eventBus.post(event);
	}
}
