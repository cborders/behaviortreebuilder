package productions.moo.behavior.events;

import com.google.common.eventbus.EventBus;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = EventBusModule.class)
public interface EventBusComponent
{
	EventBus provideEventBus();
}
