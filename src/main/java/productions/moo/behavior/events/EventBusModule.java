package productions.moo.behavior.events;

import com.google.common.eventbus.EventBus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class EventBusModule
{
	@Provides @Singleton
	static EventBus provideEventBus() {
		return new EventBus();
	}
}
