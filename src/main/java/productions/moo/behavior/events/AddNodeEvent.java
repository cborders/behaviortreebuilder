package productions.moo.behavior.events;

import productions.moo.behavior.nodes.RenderNode;

public class AddNodeEvent
{
	public RenderNode node;

	public AddNodeEvent(RenderNode node)
	{
		this.node = node;
	}
}
