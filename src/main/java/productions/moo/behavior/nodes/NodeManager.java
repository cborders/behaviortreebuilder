package productions.moo.behavior.nodes;

import com.google.common.eventbus.Subscribe;
import com.sun.org.apache.regexp.internal.RE;

import java.awt.Point;
import java.util.LinkedHashMap;
import java.util.Map;

import productions.moo.behavior.events.AddNodeEvent;
import productions.moo.behavior.events.CreateNodeEvent;
import productions.moo.behavior.events.MooEventBus;
import productions.moo.behavior.nodes.composite.CompositeNode;
import productions.moo.behavior.nodes.composite.ParallelNode;
import productions.moo.behavior.nodes.composite.SelectorNode;
import productions.moo.behavior.nodes.composite.SequenceNode;
import productions.moo.behavior.nodes.decorator.DecoratorNode;
import productions.moo.behavior.nodes.decorator.InverterNode;
import productions.moo.behavior.nodes.decorator.RepeatUntilFailureNode;
import productions.moo.behavior.nodes.decorator.RepeaterNode;
import productions.moo.behavior.nodes.decorator.SucceederNode;
import productions.moo.behavior.nodes.leaf.LeafNode;

public class NodeManager
{
	// FIXME: When I figure out dagger
	private static NodeManager _instance;
	public static NodeManager getInstance()
	{
		if(_instance == null)
		{
			_instance = new NodeManager();
		}

		return _instance;
	}

	private MooEventBus _eventBus = MooEventBus.getInstance();
	private Map<String, RenderNode> _nodes = new LinkedHashMap();

	private NodeManager()
	{
		_eventBus.register(this);

		Point pos = new Point(0, 0);
		addNodeToMap(new ParallelNode(pos));
		addNodeToMap(new SelectorNode(pos));
		addNodeToMap(new SequenceNode(pos));
		addNodeToMap(new InverterNode(pos));
		addNodeToMap(new RepeaterNode(pos));
		addNodeToMap(new RepeatUntilFailureNode(pos));
		addNodeToMap(new SucceederNode(pos));
	}

	public String[] getNodeNames()
	{
		String[] names = new String[_nodes.size()];
		_nodes.keySet().toArray(names);
		return names;
	}

	public RenderNode createNode(String name)
	{
		RenderNode node = _nodes.get(name);
		if(node != null)
		{
			node = node.clone();
		}

		return node;
	}

	@Subscribe
	public void createNode(CreateNodeEvent event)
	{
		switch(event.nodeType)
		{
			case "Leaf":
			{
				_nodes.put(event.fullname, new LeafNode(new Point(0, 0), event.shortName, event.fullname, event.packageName, event.className, event.color));
			} break;
			case "Decorator":
			{
				_nodes.put(event.fullname, new DecoratorNode(new Point(0, 0), event.shortName, event.fullname, event.packageName, event.className, event.color));
			} break;
			case "Composite":
			{
				_nodes.put(event.fullname, new CompositeNode(new Point(0, 0), event.shortName, event.fullname, event.packageName, event.className, event.color));
			} break;
			default:
			{

			} break;
		}

		RenderNode node = createNode(event.fullname).setPosition(new Point(200, 200));
		_eventBus.post(new AddNodeEvent(node));
	}

	private void addNodeToMap(RenderNode node)
	{
		_nodes.put(node.getFullName(), node);
	}
}
