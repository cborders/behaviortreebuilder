package productions.moo.behavior.nodes.composite;

import java.awt.Color;
import java.awt.Point;

public class SequenceNode extends CompositeNode
{
	public SequenceNode (Point position)
	{
		super(position, "~", "Sequence", "productions.moo.behaviortrees.compositenodes", "SequenceNode", new Color(0x19F0EE));
	}
}
