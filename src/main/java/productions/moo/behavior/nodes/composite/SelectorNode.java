package productions.moo.behavior.nodes.composite;

import java.awt.Color;
import java.awt.Point;

public class SelectorNode extends CompositeNode
{
	public SelectorNode (Point position)
	{
		super(position, "?", "Selector", "productions.moo.behaviortrees.compositenodes", "SelectorNode", new Color(0xE202F0));
	}
}
