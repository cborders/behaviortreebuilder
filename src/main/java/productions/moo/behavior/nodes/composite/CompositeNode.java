package productions.moo.behavior.nodes.composite;

import java.awt.Color;
import java.awt.Point;

import productions.moo.behavior.nodes.RenderNode;

public class CompositeNode extends RenderNode
{
	public CompositeNode (Point position, String shortName, String fullName, String packageName, String className, Color color)
	{
		super(position, shortName, fullName, packageName, className, Shape.SQUARE, color);
	}

	@Override
	public RenderNode clone()
	{
		return new CompositeNode(getPosition(), getShortName(), getFullName(), getPackage(), getClassname(), getColor());
	}
}
