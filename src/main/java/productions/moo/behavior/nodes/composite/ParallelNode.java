package productions.moo.behavior.nodes.composite;

import java.awt.Color;
import java.awt.Point;

public class ParallelNode extends CompositeNode
{
	public ParallelNode (Point position)
	{
		super(position, "||", "Parallel", "productions.moo.behaviortrees.compositenodes", "ParallelNode", new Color(0xF08D01));
	}
}
