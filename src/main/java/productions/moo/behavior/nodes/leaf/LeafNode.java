package productions.moo.behavior.nodes.leaf;

import java.awt.Color;
import java.awt.Point;

import productions.moo.behavior.nodes.RenderNode;

public class LeafNode extends RenderNode
{
	public LeafNode (Point position, String shortName, String fullName, String packageName, String className, Color color)
	{
		super(position, shortName, fullName, packageName, className, Shape.CIRCLE, color);
	}

	@Override
	public RenderNode clone()
	{
		return new LeafNode(getPosition(), getShortName(), getFullName(), getPackage(), getClassname(), getColor());
	}

	@Override
	public final void addChild(RenderNode node)
	{
		// DON'T DO IT
	}
}
