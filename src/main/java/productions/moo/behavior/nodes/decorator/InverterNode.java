package productions.moo.behavior.nodes.decorator;

import java.awt.Color;
import java.awt.Point;

public class InverterNode extends DecoratorNode
{
	public InverterNode (Point position)
	{
		super(position, "!", "Inverter", "productions.moo.behaviortrees.decoratornodes", "InverterNode", new Color(0xF0EF38));
	}
}
