package productions.moo.behavior.nodes.decorator;

import java.awt.Color;
import java.awt.Point;

public class RepeaterNode extends DecoratorNode
{
	public RepeaterNode (Point position)
	{
		super(position, "↻", "Repeater", "productions.moo.behaviortrees.decoratornodes", "RepeaterNode", new Color(0x5C52F0));
	}
}
