package productions.moo.behavior.nodes.decorator;

import java.awt.Color;
import java.awt.Point;

import productions.moo.behavior.nodes.RenderNode;

public class DecoratorNode extends RenderNode
{
	public DecoratorNode (Point position, String shortName, String fullName, String packageName, String className, Color color)
	{
		super(position, shortName, fullName, packageName, className, Shape.DIAMOND, color);
	}

	@Override
	public RenderNode clone()
	{
		return new DecoratorNode(getPosition(), getShortName(), getFullName(), getPackage(), getClassname(), getColor());
	}

	@Override
	public void addChild(RenderNode node)
	{
		if(getChildren().size() == 0)
		{
			super.addChild(node);
		}
	}
}
