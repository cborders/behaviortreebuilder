package productions.moo.behavior.nodes.decorator;

import java.awt.Color;
import java.awt.Point;

public class RepeatUntilFailureNode extends DecoratorNode
{
	public RepeatUntilFailureNode (Point position)
	{
		super(position, "*", "RepeatUntilFailure", "productions.moo.behaviortrees.decoratornodes", "RepeatUntilFailureNode", new Color(0xF04051));
	}
}
