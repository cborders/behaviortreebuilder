package productions.moo.behavior.nodes.decorator;

import java.awt.Color;
import java.awt.Point;

public class SucceederNode extends DecoratorNode
{
	public SucceederNode (Point position)
	{
		super(position, "+", "Succeeder", "productions.moo.behaviortrees.decoratornodes", "SucceederNode", new Color(0x45F02B));
	}
}
