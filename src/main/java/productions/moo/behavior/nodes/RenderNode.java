package productions.moo.behavior.nodes;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Stroke;
import java.util.ArrayList;
import java.util.List;

public class RenderNode
{
	private static final int RADIUS = 20;
	private static final Font FONT = new Font("Arial Black", Font.BOLD, 20);

	public enum Shape
	{
		CIRCLE,
		SQUARE,
		DIAMOND
	}

	private Point _position;
	private String _shortName;
	private String _fullName;
	private String _packageName;
	private String _className;
	private Shape _shape;
	private Color _color;

	private Rectangle _bounds = new Rectangle();

	private boolean _selected = false;

	private List<RenderNode> _children = new ArrayList<>();

	public RenderNode(Point position, String shortName, String fullName, String packageName, String className, Shape shape, Color color)
	{
		_position = position;
		_shortName = shortName;
		_fullName = fullName;
		_packageName = packageName;
		_className = className;
		_shape = shape;
		_color = color;
		calculateBounds();
	}

	public RenderNode setPosition(Point position)
	{
		_position = position;
		calculateBounds();
		return this;
	}

	public RenderNode translate(Point delta)
	{
		_position.x += delta.x;
		_position.y += delta.y;
		calculateBounds();
		return this;
	}

	protected String getShortName() { return _shortName; }
	protected Color getColor() { return _color; }
	protected Shape getShape() { return _shape; }

	public Point getPosition() { return _position; }
	public String getFullName() { return _fullName; }
	public String getPackage() { return _packageName; }
	public String getClassname() { return _className; }

	public boolean isSelected () { return _selected; }
	public void setSelected (boolean selected) { _selected = selected; }

	public boolean contains(Point point) { return _bounds.contains(point); }

	public void addChild(RenderNode node) { _children.add(node); }
	public List<RenderNode> getChildren() { return _children; }

	public RenderNode clone()
	{
		return new RenderNode(_position, _shortName, _fullName, _packageName, _className, _shape, _color);
	}

	public void draw (Graphics2D graphics2D)
	{
		graphics2D.setColor(_color);
		switch (_shape)
		{
			case SQUARE:
				graphics2D.fillRect(_bounds.x, _bounds.y, _bounds.width, _bounds.height);
				break;
			case CIRCLE:
				graphics2D.fillOval(_bounds.x, _bounds.y, _bounds.width, _bounds.height);
				break;
			case DIAMOND:
				graphics2D.fillRoundRect(_bounds.x, _bounds.y, _bounds.width, _bounds.height, RADIUS, RADIUS);
				break;
		}

		graphics2D.setFont(FONT);
		graphics2D.setColor(Color.darkGray);
		FontMetrics metrics = graphics2D.getFontMetrics(FONT);

		int fontBottom = (int)(_bounds.getCenterY() + metrics.getHeight() / 4f);
		int fontLeft = (int)(_bounds.getCenterX() - metrics.stringWidth(_shortName) / 2f);
		graphics2D.drawString(_shortName, fontLeft, fontBottom);

		if (_selected)
		{
			Stroke oldStroke = graphics2D.getStroke();
			graphics2D.setStroke(new BasicStroke(2));
			graphics2D.setColor(Color.darkGray);
			switch (_shape)
			{
				case SQUARE:
					graphics2D.drawRect(_bounds.x, _bounds.y, _bounds.width, _bounds.height);
					break;
				case CIRCLE:
					graphics2D.drawOval(_bounds.x, _bounds.y, _bounds.width, _bounds.height);
					break;
				case DIAMOND:
					graphics2D.drawRoundRect(_bounds.x, _bounds.y, _bounds.width, _bounds.height, RADIUS, RADIUS);
					break;
			}
			graphics2D.setStroke(oldStroke);
		}
	}

	private void calculateBounds()
	{
		_bounds.setBounds(_position.x - RADIUS, _position.y - RADIUS, 2 * RADIUS, 2 * RADIUS);
	}
}
