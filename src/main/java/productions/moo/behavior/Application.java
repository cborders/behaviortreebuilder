package productions.moo.behavior;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import productions.moo.behavior.view.EditorWindow;

public class Application
{
	public static void main(String[] args)
	{
		try
		{
			// TODO: Read app name from the Gradle file
			if (System.getProperty("os.name").contains("Mac")) {
				System.setProperty("apple.awt.application.name", "Behavior Builder");
				System.setProperty("apple.laf.useScreenMenuBar", "true");
			}
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		new EditorWindow();
	}
}
