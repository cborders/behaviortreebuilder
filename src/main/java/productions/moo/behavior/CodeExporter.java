package productions.moo.behavior;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import productions.moo.behavior.nodes.RenderNode;

public class CodeExporter
{
	public static void export(RenderNode root, String path, String className)
	{
		try(PrintWriter out = new PrintWriter(path + "/" + className + ".java"))
		{
			// TODO: Have the user dictate the package
			out.println("package productions.moo.wibbles.behavior;");
			out.println();

			// Build the import list
			List<String> imports = new ArrayList<>();
			buildImports(root, imports);
			imports.sort(String::compareTo);

			for(String importName : imports)
			{
				out.println("import " + importName + ";");
			}

			out.println();

			out.println("public class " + className + "{");
			out.println("\tpublic " + className + "() {");

			buildTree("\t\t", 0, root, out);

			// End Constructor
			out.println("\t}");
			// End Class
			out.println("}");

			out.flush();
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
	}

	private static void buildImports(RenderNode node, List<String> imports)
	{
		String importName = node.getPackage() + "." + node.getClassname();
		if(!imports.contains(importName))
		{
			imports.add(importName);
		}

		for(RenderNode child : node.getChildren())
		{
			buildImports(child, imports);
		}
	}

	private static int buildTree(String prefix, int index, RenderNode node, PrintWriter out)
	{
		index++;
		int currentNode = index;
		String currentNodeName = node.getFullName().substring(0, 1).toLowerCase() + node.getFullName().substring(1);
		currentNodeName = currentNodeName + "Node" + currentNode;
		out.print(prefix + node.getClassname() + " ");
		out.print(currentNodeName + " = new " + node.getClassname() + "();");
		out.println();

		int childIndex = currentNode;
		for(RenderNode child : node.getChildren())
		{
			int nextChild = buildTree(prefix, childIndex++, child, out);
			String childNodeName = child.getFullName() + "Node" + childIndex;
			out.println(prefix + currentNodeName + ".addNode(" + childNodeName + ");");
			childIndex = nextChild;
		}

		return childIndex;
	}
}
